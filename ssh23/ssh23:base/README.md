# DOCKER CONTAINER: PAM:LDAP

### Paquets a instal·lar en el Dockerfile:
```
apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
```

## 1.Comprovar que podem accedir a LDAP:
```
root@ldap:/opt/docker# nmap ldap.edt.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-01-17 18:03 UTC
Nmap scan report for ldap.edt.org (172.21.0.3)
Host is up (0.0000030s latency).
Not shown: 998 closed tcp ports (reset)
PORT    STATE SERVICE
389/tcp open  ldap
636/tcp open  ldapssl
```
## 2.Editar el fitxer **/etc/ldap/ldap.conf**

+ Canviar base
BASE dc=edt,dc=org

+ Canviar uri
URI ldap://ldap.edt.org 

## 3.Comprovem si tenim connectivitat client:
```
root@ldap:/opt/docker# ldapsearch -x
# extended LDIF
#
# LDAPv3
# base <dc=edt,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# edt.org
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

...
```

## 4.Configurar nsswith per decidir am quines autentificacions entrar:
Editem el fitxer **/etc/nsswitch.conf**:

```
root@ldap:/opt/docker# cat /etc/nsswitch.conf 
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```
únicament afegim ldap a passwd i group

## 5.Configura el fitxer **nslcd.conf** (dimoni):

Canviem URI i la BASE search base)
```
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org 

# The search base that will be used for all queries.
base dc=edt,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
```

## 6.Activar els dimonis:
### 6.1.Dimoni nscd
```
/usr/sbin/nscd
```

### 6.2.Dimoni nslcd
```
/usr/sin/nslcd
```

## 7.Comprovar que **getent** funciona amb usuaris ldap:
```
root@pam:/opt/docker# getent passwd pere
pere:*:5001:600:Pere Pou:/tmp/home/pere:
```

## 8.Crear directori home automàticament si no existeix:
**/etc/pam.d/common-session**: afegim l'opció que al iniciar sessió amb un usuari es creei el home.
Linea de **pam_mkhomedir.so**
```
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session optional	pam_mkhomedir.so
session	optional	pam_mount.so 
session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```

### 8.1.Comprovació:
#### Per comprovar que es crea el directori home:

Si ho fem desde root directament es salta l'autentificació
+ Entrem a l'usuari **unix01**:
```
root@pam:/opt/docker# su - unix01
```
+ Entrem a l'usuari pere i mirem si es crea el seu home: 
```
unix01@pam:~$ su - pere
Password: 
Creating directory '/tmp/home/pere'.
```

En el cas de que no tinguesim l'usuari unix:
```
for user in unix01 unix02 unix03 unix04 unix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done
```

## 9.Crear recurs temporar als usuaris LDAP
#### Als Usuaris LDAP se'ls ha de crear un recurs temporal anomenat **tmp** dins del home. </br> un ramdisk de tipus tmpfs de 100M.

Editem el fitxer **/etc/security/pam_mount.conf.xml**:
```
<!-- Volume definitions -->
<volume
	user="uid"
	fstype="tmpfs"
	options="size=100M,uid=%(USER),mode=0770"
	mountpoint="~%(USER)/home/tmp"
/>
```

### docker build --no-cache -t martireche15/pam23:ldap .

## CONTAINER LDAP
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d martireche15/ldap23:grups
```

## CONTAINER PAM
```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -d martireche15/pam23:ldap sleep infinity
```
