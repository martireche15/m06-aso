# PRÀCTICA LDAP GRUPS

## Guió Pràctica

##### Crear una nova imatge ldap: *martireche15/ldap23:grups*
+ Fer també que sigui la imatge latest *martireche15/ldap23:latest*
+ Modificar el fitxer edt.org.ldif per afegir una ou grups.
+ Definir els següents grups:
    + alumnes(600)
    + professors(601)
    + 1asix(610)
    + 2asix(611)
    + sudo(27)
    + 1wiam(612)
    + 2wiam(613)
    + 1hiaw(614)
+ Els grups han de ser elements posixGroup
+ Verificar el llistat dels usuaris i grups i la coherència de dades entre
els usuaris que ja teníem i els nous grups creats.
+ Modificar el startup.sh perquè el servei ldap escolti per tots els
protocols: ldap ldaps i ldapi.

---

## Passos a seguir:

### 1.Modificar RDN perquè els usuaris s'identifiquin amb UID
```
dn: uid=pau,ou=usuaris,dc=edt,dc=org 
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 601
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/
```

### 2.Afegir ou=grups
```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectclass: organizationalunit
```

### 3.Creació dels grups(posixGrup) 

#### alumnes(600)
```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup de alumnes
memberUid: anna
memberUid: marta
memberUid: jordi
```

#### professors(601)
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors
memberUid: pau
memberUid: pere
memberUid: jordi
```

#### 1asix(610)
```
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Grup de 1asix
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05  
```

#### 2asix(611)
```
dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
```

#### sudo(27)
```
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: master
gidNumber: 27
description: Grup de sudo
memberUid: admin
```

#### 1wiam(612)
```
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam
```

#### 2wiam(613)
```
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam
```

#### 1hiaw(614)
```
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw
```

### 4.Modificar user admin perquè sigui del grup 27 sudo
```
dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema jefazo
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3
```

### 5.Utilitzar els includes essencials del fitxer *slapd.conf*
```
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
```

### 6.Modificar *startup.sh* 
#### Afegir protols ldap/ldaps/ldapi
```
#! /bin/bash

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'
```

### 7.Execució container
```
docker build -t martireche15/ldap23:grups

docker run --rm -h ldap.edt.org --network 2hisx -p 389:389 -p 636:636 --name ldap.edt.org -d martireche15/ldap23:grups

docker exec -it ldap.edt.org /bin/bash
```

---

### COMPROVACIÓ
#### Grups
```
ldapsearch -xv -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'ou=grups,dc=edt,dc=org'
```
#### alumnes
```
ldapsearch -x -LLL -H ldap://172.19.0.2 -b 'dc=edt,dc=org' 'gidNumber=600'
```
---







