# PRÀCTICA LDAP SCHEMA

## Contingut de la pràctica:
> #### Crear una nova imatge docker anomenada: ldap23:practica
> - Pujar-la al git
> - Pijar-la al dockerhub
> - Generar els README.md apropiats
> - Crear un schema amb:
> 	- Un nou objecte STRUCTURAL
>	- Un nou objecte AUXILIARU
> 	- Cada objecte ha de tenir almenys 3 nous atributs.
>	- Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i binary per contenir documents pdf.
> - Crear una nova ou anomenada practica.
> - Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 
> 	- Assegurar-se de omplir amb dades reals la foto i el pdf.
> - Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles
> - Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.
>

## Creació schema

**animal.schema**
```schema
# animal.schema 
# 
# x-animal:
# 	x-nom
# 	x-descripcio
# 	x-foto
# 	x-pdf
#
# x-caracteristiques:
# 	x-habitat 
#	x-domestic 
#	x-longitud (cm)
#	x-pes (hg)
#
#-------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom del animal'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.2 NAME 'x-descripcio'
  DESC 'Descripció del animal'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.3 NAME 'x-foto'
  DESC 'Imatge del animal'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)

attributetype (1.1.2.1.1.4 NAME 'x-pdf'
  DESC 'Pdf'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
  SINGLE-VALUE)

attributetype (1.1.2.2.1.1 NAME 'x-habitat'
  DESC 'Habitat del animal'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.2.1.2 NAME 'x-domestic'
  DESC 'És o no és un animal domestic'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)

attributetype (1.1.2.2.1.3 NAME 'x-longitud'
  DESC 'Indica la longitud mitjana del animal en cm'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE)

attributetype (1.1.2.1.1.4 NAME 'x-pes'
  DESC 'Indica el pes mig del animal en kg'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE )

#-------------------------------------

objectClass (1.1.2.1.1 NAME 'x-animal'
  DESC 'Animal'
  SUP TOP
  STRUCTURAL
  MUST x-nom
  MAY ( x-descripcio $ x-foto $ x-pdf ))

objectClass (1.1.2.2.1 NAME 'x-caracteristiques'
  DESC 'Caracteristiques del animal'
  SUP TOP 
  AUXILIARY
  MUST ( x-serie $ x-alive )
  MAY x-poder)
```

## Creació Organitzational Unit

**edt-org.ldif**
```
# Crear l'organització
dn: dc=edt,dc=org
objectClass: top
objectClass: dcObject
objectClass: organization
o: edt.org
dc: edt

# Crear l'OU
dn: ou=practica,dc=edt,dc=org
objectClass: top
objectClass: organizationalunit
ou: practica
description: unitat organitzativa

dn: x-nom=lleopard,ou=practica,dc=edt,dc=org
objectClass: x-animal
objectClass: x-caracteristiques
x-nom: lleopard
x-descripcio: Mamífer carnívor del subordre dels fissípedes de la família dels fèlids.
x-foto:< file:/opt/docker/lleopard.jpeg
x-pdf:< file:/opt/docker/lleopard.jpeg
x-haitat: bosc tropical
x-domestic: False
x-longitud: 150
x-pes: 31

dn: x-nom=gos,ou=practica,dc=edt,dc=org
objectClass: x-animal
objectClass: x-caracteristiques
x-nom: gos
x-descripcio: Mamífer domèstic d'aspecte, forma, tamany, colors molt variables segons les races.
x-foto:< file:/opt/docker/gos.jpeg
x-pdf:< file:/opt/docker/gos.jpeg
x-haitat: viu amb els humans
x-domestic: True
x-longitud: 80
x-pes: 30

dn: x-nom=tauró,ou=practica,dc=edt,dc=org
objectClass: x-animal
objectClass: x-caracteristiques
x-nom: tauró
x-descripcio: Pez cartilaginoso que habita en el mar
x-foto:< file:/opt/docker/tauró.jpeg
x-pdf:< file:/opt/docker/tauró.pdf
x-habitat: mars i oceans
x-domestic: False
x-longitud: 500
x-pes: 250
```

## Execució
### Creació imatge
```
docker build -t martireche15/ldap23:practica .
```

### Iniciar container
```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -it martireche15/ldap23:practica /bin/bash
# bash startup.sh
```

### Comprobem que s'han injerit les dades
```
docker exec -it ldap /bin/bash/
# ldapsearch -x -LLL dn cn
``` 

### Iniciar phpldapadmin
```
docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/ldap23:phpldapadmin 

Login DN: cn=Sysadmin,cn=config
Password: syskey
```

En la terminal:

docker exec it ldap.edt.org /bin/bash

slapcat o ldapsearch -x -LLL -D 'cn=Manager,dc=edt,dc=org' -w secret -b 'ou=practica,dc=edt,dc=org' x-nom





