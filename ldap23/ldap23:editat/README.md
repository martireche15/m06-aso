# LDAP23 editat

## Afegir usuaris (usuaris1-11) i nova ou (meow)

Afegim la nova ou=meow i els usuaris usuari0-11 al fitxer **edt-org.ldif**
```
dn: ou=meow,dc=edt,dc=org
ou: meow
description: Container per meow
objectclass: organizationalunit

dn: cn=usuari0,ou=meow,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: usuari0
sn: user0
uid: usuari0
uidNumber: 9000
gidNumber: 2000
homeDirectory: /tmp/home/usuari0
```

## Modifica RDN per uid
Per a realitzar-ho, creem un fitxer anomenat **modrdn.ldif**
```
dn: cn=usuari0,ou=meow,dc=edt,dc=org
changetype: modrdn
newrdn: uid=user0
deleteoldrdn: 0
```
```
ldapmodify -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f modrdn.ldif
```

## Canviar PASSWORD (codificada)
```
root@ldap:/opt/docker# slappasswd  -h {MD5}
New password:
Re-enter new password:
```

```
docker run --rm --name ldap23:editat -h ldap.edt.org --net 2hisx martireche15/ldap23:editat 
```



