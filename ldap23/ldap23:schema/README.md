# LDAP server
## @edt ASIX M06-ASO Curs 2023-2024

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

vim futbolista.schema
```
attributetype (1.1.2.1.1.1 NAME 'x-equip' 
  DESC 'nom del equip'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 
  SINGLE-VALUE )

attributetype (1.1.2.1.1.2 NAME 'x-dorsal'
  DESC 'dorsal del jugador'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE )

attributetype (1.1.2.1.1.3 NAME 'x-web'
  DESC 'pagina(s) web del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype (1.1.2.1.1.4 NAME 'x-foto'
  DESC 'foto(s) del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.1.5 NAME 'x-lesionat'
  DESC 'lesionat TRUE/FALSE'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 )

objectClass (1.1.2.2.1.1 NAME 'x-futbolista'
  DESC 'Futboleros Crazy'
  SUP inetOrgPerson
  MUST x-equip
  MAY (x-dorsal $ x-web $ x-foto $ x-lesionat)
  )  
```





```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -it martireche15/ldap23:schema /bin/bash

bash startup.sh
```

```
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d martireche15/phpldapadmin
```

```
localhost/phpldapadmin
  cn=Sysadmin,cn=config
  syskey
schema
``` 
