# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Maig 2024
# -------------------------------------
import sys,socket,os,signal,argparse, time
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""PSax server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llistaPeers=[]
HOST = ''
PORT = args.port  
MYOEF = bytes(chr(4), 'utf-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

pid=os.fork()
if pid !=0:
  print("Engegat el server PS data:", pid)
  sys.exit(0)
  
while True: # per a cada client
  conn, addr = s.accept()
  print("Connected by", addr)
    
  while True: # per a cada ordre
    data = conn.recv(1024)
    if not data: break
    pipeData = Popen(data,stdout=PIPE,shell=True)
    for line in pipeData.stdout:
      conn.sendall(line)
    conn.send(MYOEF)
  conn.close()


