#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
#
# head [-n nlin] [-f file]
# default=10, file o stdin
# --------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
  """Mostrar les N primeres linies""",\
  epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
  help="Numero de linies (10)",dest="nlin",\
  metavar="numlinies",default=10)
parser.add_argument("-f","--file",type=str,\
  help="fitxer a processar (stdin)",metavar="file",\
  default="/dev/stdin",dest="fitxer")

args=parser.parse_args()
print(args)
exit(0)
# --------------------------------

MAX=args.nlin
fileIn=open(args.fitxer,"r")

cont=0 
for line in fileIn:
    cont+=1
    print(line,end="")
    if cont==MAX: break
fileIn.close()

exit(0)
