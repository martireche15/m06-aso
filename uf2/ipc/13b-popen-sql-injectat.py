#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# @edt ASIX M06 Curs 2023-2024
# programa.py ruta
# -------------------------------
import sys
import argparse
from subprocess import Popen, PIPE

# ------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")
parser.add_argument("sqlStatement",type=str,\
        help="consulta a la base de dades training")
args=parser.parse_args()
# --------------------------
command = "PGPASSWORD=password psql -qtA -F',' -h localhost -U postgres training" 

# estem definint el subproces
pipeData = Popen(command, shell=True, bufsize=0, universal_newlines=True,\
	stdout=PIPE, stdin=PIPE, stderr=PIPE)

# el subprocess envia a l'entrada a traves del pipe el select
# \n -> enter
# \q \n -> s'acaba el subproces 
pipeData.stdin.write(args.sqlStatement+"\n\q\n")

# llegir la informació que genera el subproces 
for line in pipeData.stdout:
    print(line,end="")


exit(0)


