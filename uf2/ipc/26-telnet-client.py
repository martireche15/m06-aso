# /usr/bin/python3
#-*- coding: utf-8-*-
# ps-client.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Maig 2024
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""ps-ax Client""")
parser.add_argument("-s","server",type=str, required=True, metavar="server")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
MYOEF = bytes(chr(4), 'utf-8')
# ------------------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True: # per a cada ordre
    command = input("~$ ")
    if not command: break
    s.sendall(bytes(command, 'utf-8'))
    
    while True: # mentre revi dades
        data = s.recv(1024)
        if data[-1:] == MYOEF:
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)






