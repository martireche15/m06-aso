# /usr/bin/python3
# -*- coding: utf-8-*-
#
# signal-exemple.py
# ------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2023
# ------------------------------------
import sys, os, signal

def hola(signum, frame):
    print("Hola!")

def adeu(signum, frame):
    print("Adeu Marrameu!")
    sys.exit(0)

print("Hola, començament del programa principaql")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    #os.wait()
    print("Programa pare: ",os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

print("Programa fill: ",os.getpid(), pid)
signal.signal(signal.SIGUSR1,hola) #10
signal.signal(signal.SIGUSR2,adeu) #12
while True:
    pass

# aixó no s'executarà mai 
print("Hasta luego Lucas!")
sys.exit(0)
