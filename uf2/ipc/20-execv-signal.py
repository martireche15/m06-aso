# /usr/bin/python3
# -*- coding: utf-8-*-
#
# signal-exemple.py
# ------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2023
# ------------------------------------
import sys,os
print("Hola, començament del programa principaql")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa pare: ",os.getpid(), pid)
    sys.exit(0)

os.execle("/usr/bin/python3", ["/usr/bin/python3", "16-signal.py", "70"])

# No s'executa mai
print("Hasta luego Lucas!")
sys.exit(0)
