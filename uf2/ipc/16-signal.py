# /usr/bin/python3
# -*- coding: utf-8-*-
#
# signal-exemple.py
# ------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2023
# ------------------------------------
# Valor inicial com a argument
# USR1 +60s
# USR2 -60s
# HUP: reinicia alarma al valor inicial
# TERM: temps que queda
# ALARM: quantes vegades hem fet upper, down i quant temps queda
# No permet el ^C 
#------------------------------------
import sys, os, signal, argparse

parser = argparse.ArgumentParser(description=\
        """Alarma""",\
        epilog="thats all folks")
parser.add_argument("-t","--time",type=int,\
        help="Valor inicial de l'alarma")
args=parser.parse_args()
print(args)

def upper(signum, frame):
    # afegir 60s
    global remaining_time
    remaining_time += 60
    signal.alarm(remaining_time)

def down(signum, frame):
    # treure 60s
    global remaining_time
    if remaining_time > 60:
        remaining_time -= 60
    signal.alarm(remaining_time)

def restart(signum, frame):
    # reinicia l'alarma a Valor inicial
    global remaining_time
    remaining_time = args.time
    signal.alarm(remaining_time)

def time_remaining(signum, frame):
    print(f"Temps restant: {remaining_time}s") # temps restant

def info_execution(signum, frame):
    print(f"Temps restant: {remaining_time}s") # temps restant
    print(f"Número de incrementos: {num_uppers}")
    print(f"Número de decrementos: {num_downs}")

# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,upper) #10
signal.signal(signal.SIGUSR2,down) #12
signal.signal(signal.SIGHUP,restart) #1
signal.signal(signal.SIGTERM,signal.time_remaining) #15 temps que queda
signal.signal(signal.SIGALRM,info_execution) #14 num_uppers/num_downs/time_remaining
sisignal.signal(signal.SIGINT,signal.SIG_IGN) #2 ignora ^C

remaining_time = args.time
num_uppers = 0
num_downs = 0

signal.alarm(remaining_time)
print(os.getpid())
while True:
    pass
sys.exit(0)



