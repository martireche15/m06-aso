# /usr/bin/python
#-*- coding: utf-8-*-
#
# 24-ss-server-one2one.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Gener 2024
# -------------------------------------
import sys, socket, argparse, os, signal
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Daytime server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-a","--any",type=int)
args=parser.parse_args()
HOST = ''
PORT = args.port
#-------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

peers = []

def myusr1(signum, frame):
    print("Signal handler with signal: ", signum)
    global peers
    print(f"llistat de conexions: {peers}")

def myusr2(signum, frame):
    print("Signal handler with signal: ", signum)
    global peers
    print(f"Contador de connexions: {len(peers)}")

def term(signum, frame):
    print("Signal handler with signal: ", signum)
    global peers
    print(peers)
    print(len(peers))
    sys.exit(0)

# Fork
pid=os.fork()
if pid !=0:
    print("Programa pare: ",os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

signal.signal(signal.SIGUSR1,myusr1) #10
signal.signal(signal.SIGUSR2,myusr2) #12
signal.signal(signal.SIGTERM,term) #15

while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  command = "ss -ltn"
  pipeData = Popen(command,stdout=PIPE)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()

# docker run --rm -h python --name python -v $(pwd):/test -p 55001:55001 -it python /bin/bash
# apt-get install -y nmap iproute2 ips ncat
# cd /test/




