# /usr/bin/python3
# -*- coding: utf-8-*-
#
# signal-exemple.py
# ------------------------------------
# @ edt ASIX M06 Curs 2023-2024
# Abril 2023
# ------------------------------------
import sys,os
print("Hola, començament del programa principaql")
print("PID pare: ", os.getpid())

pid=os.fork() # duplica el process, genera un nou exactamant igual (proces fill)
if pid !=0:
    #os.wait() # espera a que finalitzin els processos fill
    print("Programa pare: ",os.getpid(), pid)
else:
    print("Programa fill: ",os.getpid(), pid)
    while True:
        pass
print("Hasta luego Lucas!")
sys.exit(0)
