#! /bin/bash
# @edt ASIX-M06
# startup.sh
# ---------------------------

# CONFIG SAMBA
# Creació de xixa per als shares
# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# Copiar la configuració samba 
cp /opt/docker/smb.conf /etc/samba/smb.conf
# ----------------------------

# CONFIG PAM-LDAP
# borrem la configuració
rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml

cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
# -----------------------------

# Creació usuaris unix/samba
for user in samba01 samba02 samba03 samba04 vinicius
do	
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done
# ----------------------------

# Activar els serveis
# SAMBA
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd && echo "nmb  Ok"

# LDAP
/usr/sbin/nscd 
/usr/sbin/nslcd -d



