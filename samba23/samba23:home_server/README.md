# Pràctica SAMBA

### Executar server (background)
docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx -d martireche15/samba23:base

### Executar client (interactive)
docker run --rm --name client -h client --net 2hisx -it martireche15/samba23:base /bin/bash

**startup.sh**
```
#! /bin/bash
# @edt ASIX-M06
# startup.sh
# ---------------------------

# CONFIG SAMBA
# Creació de xixa per als shares
# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# Copiar la configuració samba 
cp /opt/docker/smb.conf /etc/samba/smb.conf
# ----------------------------

# CONFIG PAM-LDAP
# borrem la configuració
rm -rf /etc/nsswitch.conf
rm -rf /etc/nslcd.conf
rm -rf /etc/ldap/ldap.conf
rm -rf /etc/pam.d/common-session
rm -rf /etc/security/pam_mount.conf.xml

cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
# -----------------------------

# CREACIÓ USUARIS
# Cració usuaris unix
for user in unix01 unix02 unix03 unix04 unix05
do 
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

# Creació usuaris unix/samba
for user in lila roc patipla pla
do	
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

pdbedit -L
# ----------------------------

# Activar els serveis
# SAMBA
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd && echo "nmb  Ok"

# LDAP
/usr/sbin/nscd 
/usr/sbin/nslcd -d
```

**Dockerfile**
```
FROM debian:latest
LABEL author="@Marti Reche"
LABEL subject="samba pam-ldap"
RUN apt-get update

# Paquets generals
RUN apt-get install -y vim nmap tree iproute2 procps less finger passwd
# Paquets pam-ldap
RUN apt-get install -y libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
# Paquets samba
RUN apt-get install -y samba-client samba cifs-utils

RUN mkdir /opt/docker
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker 
CMD /opt/docker/startup.sh
```
